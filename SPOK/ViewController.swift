//
//  ViewController.swift
//  Dummy SPOK 1
//
//  Created by Ahmad Nur Alifullah on 06/04/21.
//

import UIKit
import MultipeerConnectivity
struct Variables{
    static var i  = Int.random(in: 1...8) //Nomor gambar yg akan ditampilkan
    static var emot = false
}

class ViewController: UIViewController, MCSessionDelegate, MCBrowserViewControllerDelegate, MCNearbyServiceAdvertiserDelegate {
    
    @IBOutlet weak var ChooseAvatar: UIImageView!
    @IBOutlet var hostButton: UIButton!
    @IBOutlet var joinButton: UIButton!
    @IBOutlet weak var EnterName: UITextField!
    
    var avatar = ""
    var i  = Int.random(in: 1...8) //Nomor gambar yg akan ditampilkan
    public var listAvatarTeman = Array<String>()
    
    var peerID: MCPeerID!
    var mcSession: MCSession!
    var mcAdvertiserAssistant: MCNearbyServiceAdvertiser!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        avatar = "avatar00\(i)"
        ChooseAvatar.image = UIImage(named: avatar)
        EnterName.delegate = self
        hostButton.isEnabled = false
        joinButton.isEnabled = false
    }
    
    override func didReceiveMemoryWarning() {
        super.viewDidLoad()
        //Do any additional setup after loading the view
        
    }
    
    @IBAction func backchoosing(_ sender: UIButton) {
        if i == 1 {
            i = 8
        }else {
            i -= 1
        }
        avatar = "avatar00\(i)"
        ChooseAvatar.image = UIImage(named: avatar)
    }
    
    @IBAction func nextchoosing(_ sender: UIButton) {
        if i == 8 {
            i = 1
        }else {
            i += 1
        }
        avatar = "avatar00\(i)"
        ChooseAvatar.image = UIImage(named: avatar)
    }
    
    
    @IBAction func hostButtonAction(_ sender: Any) {
        startHosting()
        toWaitingRoom()
    }
    @IBAction func joinButtonAction(_ sender: Any){
        joinSession()
        toWaitingRoom()
    }
    
    func toWaitingRoom(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "WaitingRoomVC")
                self.present(newViewController, animated: true, completion: nil)
    }
    func sendData(data: String){
        if mcSession.connectedPeers.count > 0 {
            if let textData = data.data(using: .utf8){
                do{
                    try mcSession.send(textData, toPeers: mcSession.connectedPeers, with: .reliable)
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    //start hosting a new room
    func startHosting(){
        mcAdvertiserAssistant = MCNearbyServiceAdvertiser(peer: peerID, discoveryInfo: nil, serviceType: "dulin-mpcon")
        mcAdvertiserAssistant.delegate = self
        mcAdvertiserAssistant.startAdvertisingPeer()
    }
    
    //join a room
    func joinSession(){
        let mcBrowser = MCBrowserViewController(serviceType: "dulin-mpcon", session: mcSession)
        mcBrowser.delegate = self
        present(mcBrowser, animated: true)
    }
    
    // MARK: - SESSION  METHODS
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState) {
        switch state {
        case MCSessionState.connected:
            print("Connected: \(peerID.displayName)")
        case MCSessionState.connecting:
            print("Connecting: \(peerID.displayName)")
        case MCSessionState.notConnected:
            print("Not connected: \(peerID.displayName)")
        @unknown default:
            fatalError()
        }
    }
    
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID) {
        // data received
        if let avatar = String(data:data, encoding: .utf8){
            DispatchQueue.main.async {
                self.listAvatarTeman.append(avatar)
            }
        }
    }
    
    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID) {
        
    }
    
    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress) {
        
    }
    
    func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?) {
        
    }
    
    // MARK: - BROWSER METHODS
    func browserViewControllerDidFinish(_ browserViewController: MCBrowserViewController) {
        dismiss(animated: true)
    }
    
    func browserViewControllerWasCancelled(_ browserViewController: MCBrowserViewController) {
        dismiss(animated: true)
    }
    
    // MARK: - ADVERTISER METHODS
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didReceiveInvitationFromPeer peerID: MCPeerID, withContext context: Data?, invitationHandler: @escaping (Bool, MCSession?) -> Void) {
        
        // accept the connection/invitation
        invitationHandler(true, mcSession)
    }
}
extension ViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        hostButton.isEnabled = true
        joinButton.isEnabled = true
        peerID = MCPeerID(displayName: UIDevice.current.name)
        mcSession = MCSession(peer: peerID, securityIdentity: nil, encryptionPreference: .required)
        mcSession.delegate = self
        UDM.shared.defaults.setValue(EnterName.text, forKey: "Name")
        textField.resignFirstResponder()
        return true
    }
}
class UDM {
    static let shared = UDM()
    let defaults = UserDefaults(suiteName: "com.test.saved.data")!
}

