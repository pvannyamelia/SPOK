//
//  PopupCodeViewController.swift
//  Dummy SPOK 1
//
//  Created by Ahmad Nur Alifullah on 07/04/21.
//

import UIKit

 class PopupCodeViewController: UIViewController {
    
    @IBOutlet weak var Popup: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Popup.layer.cornerRadius = 35
        Popup.layer.masksToBounds = true
        Popup.layer.borderColor = UIColor.black.cgColor
        Popup.layer.borderWidth = 3
        // Do any additional setup after loading the view.
    }
    

    
    @IBAction func ClosePopUp(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
