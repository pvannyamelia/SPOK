//
//  WaitingRoomVC.swift
//  SPOK
//
//  Created by Priscilla Vanny Amelia on 07/04/21.
//

import UIKit
import SwiftUI

class WaitingRoomVC: UIViewController {

    struct Waiting: View {
        var body: some View{
            Color.green.edgesIgnoringSafeArea(.all)
        }
    }
    @IBOutlet weak var SPOK: UIView!
    @IBOutlet weak var Whisper: UIView!
    @IBOutlet weak var Tebak: UIView!
    @IBOutlet weak var Family: UIView!
    @IBOutlet weak var Exit: UIButton!
    @IBOutlet weak var Start: UIButton!
    @IBOutlet weak var random: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SPOK.layer.borderColor = UIColor.black.cgColor
        SPOK.layer.borderWidth = 1
        SPOK.layer.cornerRadius = 10
        Whisper.layer.borderColor = UIColor.black.cgColor
        Whisper.layer.borderWidth = 1
        Whisper.layer.cornerRadius = 10
        Tebak.layer.borderColor = UIColor.black.cgColor
        Tebak.layer.borderWidth = 1
        Tebak.layer.cornerRadius = 10
        Family.layer.borderColor = UIColor.black.cgColor
        Family.layer.borderWidth = 1
        Family.layer.cornerRadius = 10
        Exit.layer.cornerRadius = 13
        Start.layer.cornerRadius = 7
        random.layer.cornerRadius = 7
        // Do any additional setup after loading the view.
    }
    
    @IBAction func randomButtonAction(_ sender: Any){
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
