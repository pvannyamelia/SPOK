//
//  gameplay3.swift
//  SPOK
//
//  Created by Ahmad Nur Alifullah on 10/04/21.
//

import UIKit

class gameplay3: UIViewController {

    public var gameplay2 : gameplay2?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    @IBAction func emot1(_ sender: Any) {
        UDM.shared.defaults.setValue("😀", forKey: "chat1")
        Variables.emot = true
        gameplay2?.updatelabel()
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func emot2(_ sender: Any) {
        UDM.shared.defaults.setValue("🥲", forKey: "chat1")
        Variables.emot = true
        gameplay2?.updatelabel()
        dismiss(animated: true, completion: nil)
    }
    @IBAction func emot3(_ sender: Any) {
        UDM.shared.defaults.setValue("🤣", forKey: "chat1")
        Variables.emot = true
        gameplay2?.updatelabel()
        dismiss(animated: true, completion: nil)
    }
    @IBAction func emot4(_ sender: Any) {
        UDM.shared.defaults.setValue("😅", forKey: "chat1")
        Variables.emot = true
        gameplay2?.updatelabel()
        dismiss(animated: true, completion: nil)
    }
    @IBAction func close(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
}
