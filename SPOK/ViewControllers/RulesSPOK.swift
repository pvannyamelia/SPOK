//
//  RulesSPOK1.swift
//  SPOK
//
//  Created by Zefanya Sharon on 09/04/21.
//

import UIKit

class RulesSPOK: UIViewController {

 
    @IBOutlet weak var Popup: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Popup.layer.cornerRadius = 35
        Popup.layer.masksToBounds = true
        Popup.layer.borderColor = UIColor.black.cgColor
        Popup.layer.borderWidth = 3
    
        // Do any additional setup after loading the view.
    }
    
    @IBAction func Close(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

    
//    @IBAction func Exit(_ sender: UIButton) {
//        dismiss(animated: true, completion: nil)
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
