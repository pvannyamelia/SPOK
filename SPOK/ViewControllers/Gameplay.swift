//
//  Gameplay.swift
//  SPOK
//
//  Created by Ahmad Nur Alifullah on 09/04/21.
//

import UIKit
struct Variabl{
    static var i_index  = Int.random(in: 1...8) //Nomor gambar yg akan ditampilkan
    
}
class Gameplay: UIViewController {
    
    @IBOutlet weak var quest: UILabel!
    
    @IBOutlet weak var kotak: UIView!
    @IBOutlet weak var jawabann: UITextView!
    @IBOutlet weak var jawab: UITextField!
    @IBOutlet weak var Player1name: UILabel!
    @IBOutlet weak var Player1img: UIImageView!
    @IBOutlet weak var lanjut: UILabel!
    
    let role = "Objek"
    let s_Quest = ["siapa nama artis favoritmu?","siapa nama teman dekatmu?", "siapa nama atlet favoritmu?", "siapa nama bos kamu?", "siapa nama orang yang ngeselin?" ]
    let o_Quest = ["Benda apa yang biasa kamu pakai buat memotong?","Benda apa yang kamu pakai sehari - hari?", "Nama temen kamu yang paling jahil"," barang yang sering kamu gunakan "]
    let p_Quest = ["menjambak",
                   "memulung",
                   "memikat",
                   "merayu",
                   "menghias",
                   "mengepel",
                   "meracik",
                   "memetik",
                   "Tiduran",
                   "mencuri"]
    let kt_Quest = ["Siang hari", "2 hari", "malam hari", "seharian", "sore pagi"]
    let kp_Quest = ["Tempat yang paling sering kamu kunjungi",
                    "di jalan",
                    "di kamar",
                    "di sekolah",
                    "di kelas"]
    
    override func viewDidLoad() {
        lanjut.text = ""
        super.viewDidLoad()
        kotak.layer.borderColor = UIColor.black.cgColor
        kotak.layer.borderWidth = 3
        jawab.delegate = self
        Player1img.image = UIImage(named: "avatar00\(Variables.i)")
        if let valuel = UDM.shared.defaults.value(forKey: "Name") as? String {
            Player1name.text = valuel
        }
        
        if role == "Objek"{
            let i_q  = Int.random(in: 1..<o_Quest.count)
            quest.text = o_Quest[i_q]
            Variabl.i_index = i_q
        }
        

        
    }
    
    @IBAction func enterjawab(_ sender: Any) {
        if let valuel2 = UDM.shared.defaults.value(forKey: "jawaban") as? String {
            jawabann.text = " \(valuel2) "
        }
        lanjut.text = "Lanjut"
    }
    
    
   
    

}
extension Gameplay : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        UDM.shared.defaults.setValue(jawab.text, forKey: "jawaban")
        textField.resignFirstResponder()
        return true
    }
}

