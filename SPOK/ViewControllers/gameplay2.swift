//
//  gameplay2.swift
//  SPOK
//
//  Created by Ahmad Nur Alifullah on 10/04/21.
//

import UIKit

class gameplay2: UIViewController {

    @IBOutlet weak var labelemot: UILabel!
    
    @IBOutlet weak var emot: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updatelabel()
        

        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is gameplay3 {
            let gp3 = segue.destination as? gameplay3
            gp3!.gameplay2 = self
        }
    }
    
    public  func updatelabel() {
        if let valuechat = UDM.shared.defaults.value(forKey: "chat1") as? String {
            labelemot.text = valuechat
        }
        
    }
}

