//
//  RulesSPOK3.swift
//  SPOK
//
//  Created by Zefanya Sharon on 12/04/21.
//

import UIKit

class RulesSPOK3: UIViewController {

    @IBOutlet weak var akhir: UIView!
    @IBOutlet weak var hasil: UIView!
    @IBOutlet weak var start: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        akhir.layer.cornerRadius = 35
        akhir.layer.masksToBounds = true
        akhir.layer.borderColor = UIColor.black.cgColor
        akhir.layer.borderWidth = 3
        
        hasil.layer.cornerRadius = 15
        hasil.layer.borderWidth = 3
        hasil.layer.borderColor = UIColor.black.cgColor
        
        start.layer.cornerRadius = 13
        // Do any additional setup after loading the view.
    }
    
    @IBAction func Exit(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
