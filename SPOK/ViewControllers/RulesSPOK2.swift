//
//  RulesSPOK2.swift
//  SPOK
//
//  Created by Zefanya Sharon on 12/04/21.
//

import UIKit

class RulesSPOK2: UIViewController {
    public  var WaitingRoomVC : WaitingRoomVC?
    
    @IBOutlet weak var Rules: UIView!
    @IBOutlet weak var Anu: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Rules.layer.cornerRadius = 35
        Rules.layer.masksToBounds = true
        Rules.layer.borderColor = UIColor.black.cgColor
        Rules.layer.borderWidth = 3
        
        Anu.layer.cornerRadius = 15
        Anu.layer.borderWidth = 0.75
        Anu.layer.borderColor = UIColor.black.cgColor
        // Do any additional setup after loading the view.
    }
    
    @IBAction func Tutup(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}
